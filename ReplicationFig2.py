import numpy as np
import random
import matplotlib.pyplot as plt
#Figure 2 Replication Moran Process
#Low Mutation Rate and high diffusion rate


#function to make sure the indivduals dying are not the same
def equal(die, die2):
    while die == die2:
        die2 = random.choice(range(N))
    return die2

for t in range (10):
    print("trial #: ",t)
    #population size
    N = 100  
    #Mutant population size (Mpop) and mutation rate (MU) are small
    Mpop =1
    #population at t=0 (1 intermediate mutant (represented by 1), the rest normal)
    pop = np.append(np.zeros((N-1)),[Mpop]) 
    Mu = 0.0001
    #difussion rate of fitness parameter 
    F = 0.00001
    #Fitness Threshold
    Y=0.1
    #Starting fitness parameter
    f = 0
    #mutation probability - 0 is no mutation 1 is mutation
    mutation = [0,1]
    mutprob = [1-Mu,Mu]
    #empty list to sum up number of intermediate mutants
    occurrences = [1]
    time = [0]
    #empty list to sum up the resistant mutants
    resoccurrences = [0]
    fitnessparam = [f]
    t3=[0,1000]
    fitnessthreshold  = [0.1,0.1]
    
    for i in range(1000):
        #select die and reproduce first since they can be the same individual  select other event paramters too since all selected simulatinasly
        die = random.choice(range(N))
        die2 = random.choice(range(N))
        equal(die,die2)
        reproduce = pop[random.choice(range(N))]
        reproduce2 = pop[random.choice(range(N))]
        indivdual = pop[random.choice(range(N))]
    
        #Allow for possible mutation for all indivudals assuming u=u1
        for ind in range(len(pop)):
            mutoccur = random.choices(mutation, mutprob)
            if (mutoccur==[1]) and (pop[ind]<2):
                pop[ind] = pop[ind] + 1
                
        #Event 1: Death and reproduce
        pop = np.delete(pop,die)
        pop = np.append(pop,reproduce)

        #Event 2 fitness parameter limit
        if f > Y: 
            #if individual is not resistant it dies
            if pop[die2] < 2:
                np.delete(pop,die2)
                np.append(pop, reproduce2)
                
        #Event 3 fitness parameter adjustment
        if (indivdual > 0):
            f = f + (1/N)
    
        #Event 4 fitness paramter reduced
        f =  f - F*f
    
 
        occurrences.append(np.count_nonzero(pop == 1))
        resoccurrences.append(np.count_nonzero(pop == 2))
        time.append(i+1)
        fitnessparam.append(f)
     
    #Plotting figure 2a
    plt.plot(time, occurrences)
    plt.title('Fig2.a: Number of Intermediate Mutants over Time ')
    plt.xlabel('Number of Generations')
    plt.ylabel('Number of Intermediate Mutants')
    plt.show()
    
    #Plotting figure 2b
    plt.plot(time, fitnessparam)
    plt.plot(t3,fitnessthreshold)
    plt.title('Fig2.b: Fitness parameter over time orange line is fitness threshold ')
    plt.xlabel('Number of Generations')
    plt.ylabel('Number of Intermediate Mutants')
    plt.show()
    
    #Plotting figure 2d
    plt.plot(time, resoccurrences)
    plt.title('Fig2.d: Number of Resistant Mutants over Time ')
    plt.xlabel('Number of Generations')
    plt.ylabel('Number of Resistant Mutants')
    plt.show()
    

    
    

